package com.pnr.mapprr.callbacks

import com.pnr.mapprr.models.Contributor
import com.pnr.mapprr.models.Repo
import com.pnr.mapprr.models.SearchResponse
import retrofit2.Call
import retrofit2.http.*


interface APIService {
    @GET("repos/{login}/{project}/contributors")
    fun getContributors(@Path("login") str: String, @Path("project") str2: String): Call<ArrayList<Contributor>>

    @GET("users/{login}/repos")
    fun getRepositories(@Path("login") str: String): Call<ArrayList<Repo>>

    @GET("search/repositories")
    fun searchRepository(@Query("q") str: String, @Query("sort") str2: String, @Query("order") str3: String, @Query("per_page") i: Int, @Query("page") i2: Int): Call<SearchResponse>

}