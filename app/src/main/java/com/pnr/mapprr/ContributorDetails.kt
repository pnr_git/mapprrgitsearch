package com.pnr.mapprr

import android.content.Context
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.pnr.mapprr.adapters.MainAdapter
import com.pnr.mapprr.callbacks.APIService
import com.pnr.mapprr.callbacks.RetrofitClient
import com.pnr.mapprr.models.Repo
import com.pnr.mapprr.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_contributor.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContributorDetails : AppCompatActivity() {
    lateinit var apiService: APIService
    lateinit var progressBar: ProgressBar
    lateinit var list_cn: RecyclerView
    private var collapsingToolbarLayout: CollapsingToolbarLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contributor)
        setSupportActionBar(findViewById(R.id.cttoolbar));
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout = findViewById(R.id.cntoolbar_layout) as CollapsingToolbarLayout
        collapsingToolbarLayout!!.title = intent.getStringExtra("login")
        Picasso.with(this@ContributorDetails).load(intent.getStringExtra("avatar")).transform(CircleTransform()).into(img_cprofile)
        progressBar = findViewById(R.id.cn_progressbar) as ProgressBar
        progressBar.visibility = View.VISIBLE
        list_cn = findViewById<RecyclerView>(R.id.rc_contributor)
        var img_nores = findViewById<ImageView>(R.id.img_cnnoresults)
        doBack(this@ContributorDetails, list_cn, progressBar, intent.getStringExtra("login"), img_nores);

    }



    class ResultsCt(var context: Context, var list: RecyclerView, var progress: ProgressBar, var noImg: ImageView) : Callback<ArrayList<Repo>> {
        override fun onFailure(call: Call<ArrayList<Repo>>, t: Throwable) {
            progress.visibility = View.GONE

            Log.d("#API#", "Failed" + t.message.toString());
        }

        override fun onResponse(call: Call<ArrayList<Repo>>, response: Response<ArrayList<Repo>>) {
            progress.visibility = View.GONE
            if (!response.body()!!.equals("")) {
                val mRepo = response.body() as ArrayList<Repo>
                if (mRepo.size > 0) {
                    list.layoutManager = LinearLayoutManager(context)
                    val adapter = MainAdapter(context, mRepo)
                    list.adapter = adapter
                } else {
                    noImg.visibility = View.VISIBLE
                }
            }
        }

    }

    fun doBack(ctx: Context, lsit_ct: RecyclerView, progress: ProgressBar, login: String, img_no: ImageView) {
        apiService = RetrofitClient.getClient(this@ContributorDetails)!!.create(APIService::class.java)
        this.apiService.getRepositories(login).enqueue(ResultsCt(ctx, lsit_ct, progress, img_no))
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish();
        return super.onOptionsItemSelected(item)
    }
}