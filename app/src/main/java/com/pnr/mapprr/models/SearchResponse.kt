package com.pnr.mapprr.models

data class SearchResponse(var total_count: Int, var incomplete_results: Boolean, var items: ArrayList<Repo>)