package com.pnr.mapprr.callbacks

import android.content.Context
import com.pnr.mapprr.R
import okhttp3.OkHttpClient.Builder;
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level;


class RetrofitClient {
    companion object {
        private var retrofit: Retrofit? = null
        private var uniqInstance: RetrofitClient? = null
        fun getInstance(): RetrofitClient {
            if (uniqInstance == null) {
                uniqInstance = RetrofitClient()
            }
            return uniqInstance as RetrofitClient
        }
         fun getClient(context: Context): Retrofit? {
            if (retrofit == null) {
                val httpClient = Builder()
                httpClient.connectTimeout(5, TimeUnit.MINUTES)
                httpClient.readTimeout(5, TimeUnit.MINUTES)
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = Level.BODY
                httpClient.addInterceptor(RestInterceptor())
                httpClient.addInterceptor(loggingInterceptor)
                retrofit = Retrofit.Builder().baseUrl(context.getResources().getString(R.string.base)).client(httpClient.build()).addConverterFactory(GsonConverterFactory.create()).build()
            }
            return retrofit
        }
    }

}