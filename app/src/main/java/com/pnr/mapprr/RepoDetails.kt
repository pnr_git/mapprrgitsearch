package com.pnr.mapprr

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.text.Html
import android.util.Log
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.card_descrption.*
import kotlinx.android.synthetic.main.card_projectlink.*
import android.text.Spanned
import android.view.MenuItem
import android.view.View
import android.widget.GridView
import android.widget.ProgressBar
import android.widget.TextView
import com.pnr.mapprr.adapters.ContributorAdapter
import com.pnr.mapprr.callbacks.APIService
import com.pnr.mapprr.callbacks.RetrofitClient
import com.pnr.mapprr.models.Contributor
import com.pnr.mapprr.utils.CircleTransform
import kotlinx.android.synthetic.main.card_contributors.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RepoDetails : AppCompatActivity() {
    private var collapsingToolbarLayout: CollapsingToolbarLayout? = null
    lateinit var apiService: APIService
    private val mContributors: ArrayList<Contributor>? = null
    @TargetApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        /*Picasso.with(this@RepoDetails)
                .load(intent.getStringExtra("avatar"))
                .placeholder(R.drawable.no_image)
                .into(profile_image);*/

        Picasso.with(this@RepoDetails).load(intent.getStringExtra("avatar")).transform(CircleTransform()).into(profile_image)

        //Picasso.with(this@RepoDetails).load(intent.getStringExtra("avatar")).placeholder(R.drawable.no_image).transform(CircleTransform()).into(profile_image)
        collapsingToolbarLayout = findViewById(R.id.toolbar_layout) as CollapsingToolbarLayout
        collapsingToolbarLayout!!.title = intent.getStringExtra("name")

        tv_link.setText(fromHtml("<u>" + intent.getStringExtra("url") + "</u>"))
        tv_description.setText(intent.getStringExtra("desc"))

        doBackContributors(this@RepoDetails, intent.getStringExtra("login"), intent.getStringExtra("name"), grid_progress, no_contributors, CGrid)

    }

    class DetailResults(var context: Context, var login: String, var name: String, var progress: ProgressBar, var no: TextView, var grid: GridView) : Callback<ArrayList<Contributor>> {
        override fun onFailure(call: Call<ArrayList<Contributor>>, t: Throwable) {
            progress.visibility = View.GONE
            Log.d("#API#", "Failed" + t.message.toString());
        }

        override fun onResponse(call: Call<ArrayList<Contributor>>, response: Response<ArrayList<Contributor>>) {
            progress.visibility = View.GONE
            if (!response.body()!!.equals("")) {
                val mContributors = response.body() as ArrayList<Contributor>
                val adapter = ContributorAdapter(context, mContributors)
                grid.visibility = View.VISIBLE
                grid.adapter = adapter
                val listAdapter = grid.adapter
                if (listAdapter != null) {
                    val items = adapter.getCount()
                    val listItem = listAdapter.getView(0, null, grid)
                    listItem.measure(0, 0)
                    var totalHeight = listItem.measuredHeight
                    if (items > 3) {
                        totalHeight *= (1.0f + (items / 3).toFloat()).toInt()
                    }
                    val params = grid.layoutParams
                    params.height = totalHeight
                    grid.layoutParams = params
                }
            } else {
                no.visibility = View.VISIBLE
            }
        }


    }

    fun doBackContributors(ctx: Context, login: String, name: String, progress: ProgressBar, no: TextView, grid: GridView) {
        apiService = RetrofitClient.getClient(this@RepoDetails)!!.create(APIService::class.java)
        this.apiService.getContributors(login, name).enqueue(DetailResults(ctx, login, name, progress, no, grid))
    }



    fun fromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish();
        return super.onOptionsItemSelected(item)
    }
}
