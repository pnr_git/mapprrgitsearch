package com.pnr.mapprr.callbacks

import okhttp3.Interceptor
import okhttp3.Response

class RestInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder()
        requestBuilder.header("Authorization", "token 39c15c9a9fa2f4219836643bc49303ee1cf7f710")
        requestBuilder.header("Accept", "application/vnd.github.mercy-preview+json")
        return chain.proceed(original)
    }
}