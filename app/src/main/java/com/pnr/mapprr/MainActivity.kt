package com.pnr.mapprr

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.pnr.mapprr.adapters.MainAdapter
import com.pnr.mapprr.callbacks.APIService
import com.pnr.mapprr.callbacks.RetrofitClient
import com.pnr.mapprr.models.Repo
import com.pnr.mapprr.models.SearchResponse
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.app.SearchManager
import android.support.v4.view.MenuItemCompat
import android.text.TextUtils
import android.widget.Toast
import android.support.v7.widget.SearchView
import android.widget.EditText


@Suppress("CAST_NEVER_SUCCEEDS")
class MainActivity : AppCompatActivity() {

    private val orderBy = "desc"
    private val pageNo = 1
    private val perPage = 10
    private val queryBy = "nagarajup"
    private val sortBy = "watcher"
    lateinit var apiService: APIService
    lateinit var progressBar: ProgressBar
    lateinit var img_nores: ImageView
    lateinit var mRepoList: ArrayList<Repo>
    lateinit var list_home: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        progressBar = findViewById(R.id.progressbar) as ProgressBar
        progressBar.visibility = View.VISIBLE
        list_home = findViewById<RecyclerView>(R.id.rcv_main)
        img_nores = findViewById<ImageView>(R.id.img_noresults)
        doBack(this@MainActivity, list_home, progressBar, "raj", img_nores);

        /*fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/
    }


    class Results(var context: Context, var list_home: RecyclerView, var progress: ProgressBar, var img_nores: ImageView) : Callback<SearchResponse> {

        override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
            progress.visibility = View.GONE

            Log.d("#API#", "Failed" + t.message.toString());
        }

        override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {
            progress.visibility = View.GONE

            val mRepoList = (response.body() as SearchResponse).items
            Log.e("#API#", "List items" + mRepoList);
            if (mRepoList.size > 0) {
                list_home.layoutManager = LinearLayoutManager(context)
                val adapter = MainAdapter(context, mRepoList)
                list_home.adapter = adapter
                adapter.notifyDataSetChanged()
            } else {
                img_nores.visibility = View.VISIBLE
            }
            Log.e("#API#", "Success" + response.body());
        }

    }

    fun doBack(ctx: Context, list_home: RecyclerView, progress: ProgressBar, q: String, img_no: ImageView) {
        progress.visibility = View.VISIBLE
        list_home.adapter = null;
        apiService = RetrofitClient.getClient(this@MainActivity)!!.create(APIService::class.java)
        this.apiService.searchRepository(q, this.sortBy, this.orderBy, this.perPage, this.pageNo).enqueue(Results(ctx, list_home, progress, img_no))
    }

    inner class EndlessScrollListener(private val listView: RecyclerView) : RecyclerView.OnScrollListener() {
        internal var flag = false

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {}

        override fun onScrollStateChanged(view: RecyclerView?, scrollState: Int) {
            val layoutManager = view!!.layoutManager as LinearLayoutManager
            val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
            val firstVisiblePosition = layoutManager.findFirstVisibleItemPosition()

            if (scrollState == 2) {
                flag = true
            }
            /*if (scrollState == 0
                    && lastVisiblePosition == listView.adapter.itemCount - 1 && scrollflag) {

                if (!loading) {
                    loading = true
                   doBack()
                }
            }*/
        }

    }

    lateinit var searchView: SearchView

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val searchItem = menu.findItem(R.id.action_search)

        if (searchItem != null) {
            searchView = MenuItemCompat.getActionView(searchItem) as SearchView
            searchView.setOnCloseListener(SearchView.OnCloseListener {
                true
            })
            searchView.setOnSearchClickListener(View.OnClickListener {
                //some operation
                //Toast.makeText(this@MainActivity, "Search", Toast.LENGTH_SHORT).show()
            })
            val searchPlate = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
            searchPlate.hint = "Search"
            /*val searchPlateView = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate)
            searchPlateView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))*/
            // use this method for search process
            searchView.setIconifiedByDefault(true)
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    // use this method when query submitted
                    if (!TextUtils.isEmpty(query) && query.length >= 3) {
                        doBack(this@MainActivity, list_home, progressBar, query, img_nores);
                    } else {
                        Toast.makeText(this@MainActivity, "Please Enter Valid Query", Toast.LENGTH_SHORT).show()
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    // use this method for auto complete search process
                    return false
                }
            })

            /*searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(p0: MenuItem?): Boolean {
                    Toast.makeText(this@MainActivity, "Expanded", Toast.LENGTH_SHORT).show()
                    return true
                }


                override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {
                    Toast.makeText(this@MainActivity, "Colpsed", Toast.LENGTH_SHORT).show()
                    return false
                }

            })*/

            val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true)
            searchView.onActionViewCollapsed();
            doBack(this@MainActivity, list_home, progressBar, "raj", img_nores);
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.action_search -> true

            R.id.action_filter -> {
                Toast.makeText(this@MainActivity, "filter", Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
