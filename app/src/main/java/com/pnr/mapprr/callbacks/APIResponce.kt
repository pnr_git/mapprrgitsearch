package com.pnr.mapprr.callbacks

interface APIResponse {
    abstract fun onSuccess(res: String)

    abstract fun onFailure(res: String)
}