package com.pnr.mapprr.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.pnr.mapprr.models.Contributor
import com.squareup.picasso.Picasso
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.ImageView
import com.pnr.mapprr.ContributorDetails
import com.pnr.mapprr.R
import com.pnr.mapprr.utils.CircleTransform


class ContributorAdapter(var context: Context, var contList: ArrayList<Contributor>) : BaseAdapter() {
    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val itemView = LayoutInflater.from(p2?.getContext()).inflate(R.layout.ctngrid, p2, false)
        Log.e("ABC","contList.get(p0)"+contList.get(p0))
        val contributorName = itemView.findViewById(R.id.tv_cnt_name) as TextView
        Picasso.with(context).load((this.contList.get(p0) as Contributor).avatar_url).placeholder(R.drawable.ic_avatar as Int).transform(CircleTransform()).into(itemView.findViewById(R.id.img_cnt) as ImageView)
        contributorName.text = (this.contList.get(p0) as Contributor).login

        itemView.setOnClickListener{
            val intent = Intent(context, ContributorDetails::class.java)
            intent.putExtra("avatar", contList.get(p0).avatar_url)
            intent.putExtra("login", contList.get(p0).login)
            context.startActivity(intent)
        }


        return itemView
    }

    override fun getItem(p0: Int): Any {
        return contList.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return (this.contList.get(p0) as Contributor).id.toLong();
    }

    override fun getCount(): Int {
        return contList.size
    }
}