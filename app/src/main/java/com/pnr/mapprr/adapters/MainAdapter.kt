package com.pnr.mapprr.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.pnr.mapprr.R
import com.pnr.mapprr.RepoDetails
import com.pnr.mapprr.databinding.MainAdapterBinding
import com.pnr.mapprr.models.Repo
import com.squareup.picasso.Picasso




/*https://blog.untitledkingdom.com/refactoring-recyclerview-adapter-to-data-binding-5631f239095f*/

class MainAdapter(val context: Context, val homelist: ArrayList<Repo>) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MainAdapterBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = homelist.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(homelist[position])
        /* Glide.with(context)
                 .load(homelist[position].owner.avatar_url)
                 .apply(RequestOptions()
                         .placeholder(R.drawable.no_image))
                 .into(holder.binding.ivIcon)*/

        Picasso.with(context)
                .load(homelist[position].owner.avatar_url)
                .placeholder(R.drawable.no_image)
                .into(holder.binding.ivIcon);

        holder.binding.cardHome.setOnClickListener {
            Log.e("#API#", "Data" + homelist[position])
            val repo=homelist[position] as Repo
            val intent = Intent(context, RepoDetails::class.java)
            intent.putExtra("name",  homelist[position].name)
            intent.putExtra("url",  homelist[position].html_url)
            intent.putExtra("desc",  homelist[position].description)
            intent.putExtra("login",  homelist[position].owner.login)
            intent.putExtra("avatar",  homelist[position].owner.avatar_url)
            context.startActivity(intent)
        }
    }

    inner class ViewHolder(val binding: MainAdapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(repo: Repo) {
            binding.repo = repo
            binding.executePendingBindings()

        }
    }
}